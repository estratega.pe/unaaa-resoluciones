<?php

/**
 * Fired during plugin activation
 *
 * @link       https://estratega.pe
 * @since      1.0.0
 *
 * @package    Unaaaa_Resoluciones
 * @subpackage Unaaaa_Resoluciones/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Unaaaa_Resoluciones
 * @subpackage Unaaaa_Resoluciones/includes
 * @author     MrHellmann <aisenhaim@gmail.com>
 */
class Unaaaa_Resoluciones_Activator {

	public static function activate() {
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		$table_name = $wpdb->prefix . 'unaaa_resol';

		$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			ID int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			title varchar(128) NOT NULL,
			fecha date NOT NULL DEFAULT '0000-00-00',
			content text NOT NULL,
			fid varchar(255) NOT NULL,
			tipo tinyint(4) NOT NULL,
			status tinyint(4) NOT NULL,
			created_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  			updated_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
			UNIQUE KEY ID (ID)
		) $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );

		self::unaaa_create_page();
	}

	public static function unaaa_create_page() {
		$pagename = "resoluciones";
		$unaaa_page_id= get_option( "unaaa_page_id" );
		if( !$unaaa_page_id ) {
			$the_page_title = ucfirst($pagename);
	
			$the_page = self::verify_page_title( $the_page_title );
			if ( ! $the_page ) {
				
				$_p = array();
				$_p['post_title'] = $the_page_title;
				$_p['post_content'] = "";
				$_p['post_status'] = 'publish';
				$_p['post_type'] = 'page';
				$_p['comment_status'] = 'closed';
				$_p['ping_status'] = 'closed';
				$_p['post_category'] = array(1);
				
				$the_page_id = wp_insert_post( $_p );
			}
			else {
				
				$the_page_id = $the_page->ID;
				
				$the_page->post_status = 'publish';
				$the_page_id = wp_update_post( $the_page );
				delete_option( 'unaaa_page_id' );
			}
			add_option( 'unaaa_page_id', $the_page_id );
		}
	}

	private static function verify_page_title($title) {
		$query = new WP_Query(
			array(
				'post_type'              => 'page',
				'title'                  => $title,
				'post_status'            => 'all',
				'posts_per_page'         => 1,
				'no_found_rows'          => true,
				'ignore_sticky_posts'    => true,
				'update_post_term_cache' => false,
				'update_post_meta_cache' => false,
				'orderby'                => 'post_date ID',
				'order'                  => 'ASC',
			)
		);
		 
		if ( ! empty( $query->post ) ) {
			$page_got_by_title = $query->post;
		} else {
			$page_got_by_title = null;
		}
		return $page_got_by_title;
	}

}
