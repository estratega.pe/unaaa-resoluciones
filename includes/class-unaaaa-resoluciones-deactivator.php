<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://estratega.pe
 * @since      1.0.0
 *
 * @package    Unaaaa_Resoluciones
 * @subpackage Unaaaa_Resoluciones/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Unaaaa_Resoluciones
 * @subpackage Unaaaa_Resoluciones/includes
 * @author     MrHellmann <aisenhaim@gmail.com>
 */
class Unaaaa_Resoluciones_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		global $wpdb;
		$table_name = $wpdb->prefix . 'unaaa_resol';
		$sql = "DROP TABLE IF EXISTS  $table_name";

		$wpdb->query( $sql );

		self::unaaa_remove_page();
	}

	public static function unaaa_remove_page() {
		$the_page_id = get_option( 'unaaa_page_id' );
		if( $the_page_id ) {
			wp_delete_post( $the_page_id );	
		}
		delete_option("unaaa_page_id");
	}

}
