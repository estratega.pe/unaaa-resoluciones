<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://estratega.pe
 * @since      1.0.0
 *
 * @package    Unaaaa_Resoluciones
 * @subpackage Unaaaa_Resoluciones/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Unaaaa_Resoluciones
 * @subpackage Unaaaa_Resoluciones/includes
 * @author     MrHellmann <aisenhaim@gmail.com>
 */
class Unaaaa_Resoluciones_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'unaaaa-resoluciones',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
