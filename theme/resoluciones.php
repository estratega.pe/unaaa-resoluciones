<?php
get_header(); 
?>
<!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> -->

<?php
global $wpdb;
$table_name = $wpdb->prefix . 'unaaa_resol';
// $where = "";
// $order = "created_at DESC";
// $query = "SELECT * FROM " . $table_name . $where . " order by " . $order . " LIMIT 10";
// $rows = $wpdb->get_results($query);
// echo "<pre>";
// print_r($wpdb->last_query);
// echo "<br />";
// print_r($rows);
// wp_die();
$validTypes = [2,3,24,25,26];
$searchByTipo = (isset($_GET['tipo'])) ? (int)($_GET['tipo']) : null;
$searchByTitle = (isset($_GET['title'])) ? sanitize_text_field($_GET['title']) : null;
$searchByDate = (isset($_GET['fecha'])) ? sanitize_text_field($_GET['fecha']) : null;


$whereType = "";
if(in_array($searchByTipo, $validTypes)) {
    $whereType = " AND tipo=" . $searchByTipo;
}
$whereTitle = ($searchByTitle) ? " AND title LIKE '%$searchByTitle%' " : "";
$whereFecha = "";
if($searchByDate) {
    $dateStart = date('Y-m-d', strtotime($searchByDate . '-01'));
    $dateEnd = new DateTime($dateStart . ' 23:59:59');
    $dateStart = $dateStart . ' 00:00:00';
    $dateEnd->modify('+1 month');
    $dateEnd->modify('-1 day');
    $whereFecha = " AND fecha BETWEEN '$dateStart' AND '" . $dateEnd->format('Y-m-d H:i:s') . "'";
}
$customPagHTML     = "";
$query             = "SELECT * FROM " . $table_name . " WHERE status=1 " . $whereType . $whereTitle . $whereFecha;
$total_query     = "SELECT COUNT(1) FROM ($query) AS combined_table";
$total             = $wpdb->get_var( $total_query );
$items_per_page = 10;
$page             = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;
$offset         = ( $page * $items_per_page ) - $items_per_page;
$rows         = $wpdb->get_results( $query . " ORDER BY created_at DESC LIMIT $offset, $items_per_page" );
$totalPage         = ceil($total / $items_per_page);
// wp_die($wpdb->last_query);

if($totalPage > 1){
$customPagHTML     =  /*'<div><span>Page '.$page.' of '.$totalPage.'</span>'.*/paginate_links( array(
    'base' => add_query_arg( 'cpage', '%#%' ),
    'format' => '',
    'prev_text' => __('&laquo;'),
    'next_text' => __('&raquo;'),
    'total' => $totalPage,
    'current' => $page
    )).'</div>';
}
?>

<style id="et-critical-inline-css">
            
        </style>
<div id="main-content">
    <article class="post-239850 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="et-l et-l--post">
                <div class="et_builder_inner_content et_pb_gutters3">
                    <div class="et_pb_section et_pb_section_0 et_section_regular">

                        <div class="et_pb_row et_pb_row_1 et_pb_gutters3">
                            <div class="et_pb_text_inner">
                                <form method="get">
                                <div class="row">
                                        <div class="et_pb_with_border et_pb_module et_pb_text et_pb_text_0  et_pb_text_align_left et_pb_bg_layout_light col-sm-12 col-md-4">
                                            <div class="et_pb_text_inner">
                                                <h1>Resoluciones</h1>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-3 p-3">
                                            <div class="input-group">
                                                <select name="tipo" class="form-control">
                                                    <option value="all">Todos</option>
                                                    <option value="2" <?php echo ($searchByTipo == 2) ? "selected": "";?>>Resolución Administrativa</option>
                                                    <option value="3" <?php echo ($searchByTipo == 3) ? "selected": "";?>>Resolución Presidencial.</option>
                                                    <option value="24" <?php echo ($searchByTipo == 24) ? "selected": "";?>>Resoluciones de Comisión Organizadora</option>
                                                    <option value="25" <?php echo ($searchByTipo == 25) ? "selected": "";?>>Resoluciones de Actas Ordinarias</option>
                                                    <option value="26" <?php echo ($searchByTipo == 26) ? "selected": "";?>>Resoluciones de Actas Extraordinarias</option>
                                                </select>
                                                <button type="submit" class="btn btn-success">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"></path>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-3 p-3">
                                            <div class="input-group">
                                                <input class="form-control" name="title" type="text" value="<?php echo $searchByTitle;?>" placeholder="Buscar por nombre">
                                                <button type="submit" class="btn btn-success">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"></path>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-2 p-3">
                                                <div class="input-group">
                                                    <input class="form-control" type="month" name="fecha" value="<?php echo $searchByDate;?>">
                                                    <button type="submit" class="btn btn-success">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                                            <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"></path>
                                                        </svg>
                                                    </button>
                                                </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <?php
                        foreach($rows as $row) {
                            $pdfFile = wp_get_attachment_url($row->fid);
                        ?>
                        <div class="et_pb_row et_pb_row_1 et_pb_gutters3">
                            <div class="et_pb_text_inner">
                                <div class="row">
                                    <div class="col-sm-12 col-md-10">
                                        <h4><?php echo $row->{'title'};?></h4>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <small>Publicación: <strong><?php echo date('d-m-Y', strtotime($row->{'fecha'}));?></strong></small>
                                    </div>
                                </div>
                                <div class="row">
                                    <p class="col-sm-12 col-md-10"><?php echo $row->{'content'};?></p>
                                    <a class="col-sm-12 col-md-2 text-center"href="<?php echo $pdfFile;?>" target="_blank">
                                        <img src="<?php echo plugin_dir_url( __FILE__ ).'../public/images/pdf.png';?>" width="50px" alt="Descargar <?php echo $row->{'title'};?>">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                        <div class="et_pb_row et_pb_row_1 et_pb_gutters3">
                            <?php echo $customPagHTML;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>
<?php get_footer(); ?>