<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://estratega.pe
 * @since             1.0.0
 * @package           Unaaaa_Resoluciones
 *
 * @wordpress-plugin
 * Plugin Name:       unaaa resoluciones
 * Plugin URI:        https://webperu2.com
 * Description:       unaaa resoluciones
 * Version:           1.0.0
 * Author:            MrHellmann
 * Author URI:        https://estratega.pe/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       unaaaa-resoluciones
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'UNAAAA_RESOLUCIONES_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-unaaaa-resoluciones-activator.php
 */
function activate_unaaaa_resoluciones() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-unaaaa-resoluciones-activator.php';
	Unaaaa_Resoluciones_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-unaaaa-resoluciones-deactivator.php
 */
function deactivate_unaaaa_resoluciones() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-unaaaa-resoluciones-deactivator.php';
	Unaaaa_Resoluciones_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_unaaaa_resoluciones' );
register_deactivation_hook( __FILE__, 'deactivate_unaaaa_resoluciones' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-unaaaa-resoluciones.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_unaaaa_resoluciones() {

	$plugin = new Unaaaa_Resoluciones();
	$plugin->run();

}
run_unaaaa_resoluciones();
