<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://estratega.pe
 * @since      1.0.0
 *
 * @package    Unaaaa_Resoluciones
 * @subpackage Unaaaa_Resoluciones/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Unaaaa_Resoluciones
 * @subpackage Unaaaa_Resoluciones/admin
 * @author     MrHellmann <aisenhaim@gmail.com>
 */
class Unaaaa_Resoluciones_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Unaaaa_Resoluciones_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Unaaaa_Resoluciones_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/unaaaa-resoluciones-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Unaaaa_Resoluciones_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Unaaaa_Resoluciones_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/unaaaa-resoluciones-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function admin_add_menu() {

		$menu_slug = 'unaaa-resoluciones';

        $adminUnaaaResoluciones = add_menu_page(
            __('Resoluciones')
            ,__('Resoluciones')
            ,'manage_options'
            ,$menu_slug
            ,[$this, 'unaaa_resolucion_page']
		);

		// add_submenu_page($menu_slug, 'Add New', 'Add New', 'manage_options', 'unaaa-resoluciones-add', [$this, 'unaaa_add_resolucion']);
		// add_submenu_page($menu_slug, 'Edit', 'Edit', 'manage_options', 'unaaa-resoluciones-edit', [$this, 'unaaa_edit_resolucion']);
	}

	public function unaaa_resolucion_page() {
		$action = isset($_GET['action']) ? $_GET['action'] : 'list';
		switch($action) {
			case 'edit':
				$this->unaaa_edit_resolucion();
				break;
			case 'add':
				$this->unaaa_add_resolucion();
				break;
			case 'delete':
				$this->unaaa_remove_resolucion();
				break;
			case 'list':
			default:
				$this->unaaa_list_table_init();
				break;
		}
	}

	private function unaaa_remove_resolucion() {
		if (
			$_SERVER['REQUEST_METHOD'] == 'GET' 
			&& isset($_GET['token']) 
			&& wp_verify_nonce($_GET['token'], 'unaaa_resolucion_remove')
		) {
			global $wpdb;
			$table_name = $wpdb->prefix . 'unaaa_resol';
			$id = isset($_GET['resolucion']) ? intval($_GET['resolucion']) : 0;
			$wpdb->delete($table_name, ['ID' => $id]);
			?>
				<div class="wrap">
				<h1 class="wp-heading-inline">Resoluciones</h1>
				<a href="<?php echo admin_url('admin.php?page=unaaa-resoluciones'); ?>" class="page-title-action">Regresar al listado</a>
				<hr class="wp-header-end">
				<div class="notice notice-success is-dismissible"><p>Eliminado correctamente!</p></div>
				<?php
		}
	}

	private function unaaa_edit_resolucion() {
		global $wpdb;

		$table_name = $wpdb->prefix . 'unaaa_resol';
		$id = isset($_GET['resolucion']) ? intval($_GET['resolucion']) : 0;
		$_token = isset($_GET['token']) ? $_GET['token'] : '0';
		if( wp_verify_nonce($_token, 'unaaa_resolucion_edit') ) {
			$row = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE ID = %d", $id));
			if (!$row) {
				echo '<div class="notice notice-error is-dismissible"><p>Imposible, por favor elija una resolución a editar.</p></div>';
				return;
			}
			if (
				$_SERVER['REQUEST_METHOD'] == 'POST' 
				&& isset($_POST['unaaa_resolucion_nonce']) 
				&& wp_verify_nonce($_POST['unaaa_resolucion_nonce'], 'unaaa_resolucion_nonce')
			) {
				?>
				<div class="wrap">
				<h1 class="wp-heading-inline">Resoluciones</h1>
				<a href="<?php echo admin_url('admin.php?page=unaaa-resoluciones'); ?>" class="page-title-action">Regresar al listado</a>
				<a href="<?php echo admin_url('admin.php?page=unaaa-resoluciones&action=edit&resolucion=' . $row->ID . '&token=' . wp_create_nonce('unaaa_resolucion_edit')); ?>" class="page-title-action">Editar resolución</a>
				<hr class="wp-header-end">
				<?php
				global $wpdb;

				$table_name = $wpdb->prefix . 'unaaa_resol';
	
				$title = sanitize_text_field($_POST['title']);
				$content = sanitize_text_field($_POST['content']);
				$tipo = (int)$_POST['tipo'];
				$fecha = $_POST['fecha'];
				$updated_at = current_time('mysql');

				$wpdb->update($table_name, compact('title', 'content', 'tipo', 'fecha', 'updated_at'), ['ID' => $id]);
				?>
				<div class="notice notice-success is-dismissible"><p>datos actualizados correctamente!</p></div>
				<?php

				if($_FILES['resol']['name']) {
					if(!$_FILES['resol']['error']) {
						$new_file_name = strtolower($_FILES['resol']['tmp_name']);
		
						require_once( ABSPATH . 'wp-admin/includes/image.php' );
						require_once( ABSPATH . 'wp-admin/includes/file.php' );
						require_once( ABSPATH . 'wp-admin/includes/media.php' );
		
						$fid = media_handle_upload( 'resol', 0 );
		
						if ( is_wp_error( $fid ) ) {
							wp_die('Error loading file!');
						 } else {
			   
						   $wpdb->update($table_name, compact('fid', 'updated_at'), ['ID' => $id]);
						?>
						<div class="notice notice-success is-dismissible"><p>archivo actualizado correctamente!</p></div>
						<?php
						 }
					}
				}
			} else {
				?>
				<div class="wrap">
					<h1 class="wp-heading-inline">Editar resolución <?php echo $row->title; ?></h1>
					<hr class="wp-header-end">


						<form method="post" enctype="multipart/form-data">
						<table class="form-table">
							<tr>
								<th scope="row"><label for="tipo">Tipo</label></th>
								<td>
									<select name="tipo" required>
										<option value="2" <?php echo ($row->tipo == 2)? "selected":"";?>>Resolución Administrativa</option>
										<option value="3" <?php echo ($row->tipo == 3)? "selected":"";?>>Resolución Presidencial.</option>
										<option value="24" <?php echo ($row->tipo == 24)? "selected":"";?>>Resoluciones de Comisión Organizadora</option>
										<option value="25" <?php echo ($row->tipo == 25)? "selected":"";?>>Resoluciones de Actas Ordinarias</option>
										<option value="26" <?php echo ($row->tipo == 26)? "selected":"";?>>Resoluciones de Actas Extraordinarias</option>
									</select>
								</td>
							</tr>
							<tr>
								<th scope="row"><label for="title">Nombre</label></th>
								<td><input name="title" type="text" id="title" class="regular-text" value="<?php echo esc_attr($row->title); ?>" required></td>
							</tr>
							<tr>
								<th scope="row"><label for="content">Description</label></th>
								<td><textarea name="content" required><?php echo esc_attr($row->content); ?></textarea></td>
							</tr>
							<tr>
								<th scope="row"><label for="fecha">Fecha</label></th>
								<td><input type="date" name="fecha" value="<?php echo esc_attr($row->fecha); ?>" required /></td>
							</tr>
							<tr>
								<th scope="row"><label for="resol">Archivo</label></th>
								<td><input type="file" name="resol"  /></td>
							</tr>
						</table>

						<input type="hidden" name="unaaa_resolucion_nonce" value="<?php echo wp_create_nonce('unaaa_resolucion_nonce'); ?>">
						<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Actualizar"></p>
						</form>
				</div>
				<?php
			}
		} else {
			echo '<div class="notice notice-error is-dismissible"><p>Imposible, por favor elija editar desde el listado.</p></div>';
			return;
		}
	}

	function unaaa_add_resolucion() {
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' 
			&& isset($_POST['unaaa_resolucion_nonce']) 
			&& wp_verify_nonce($_POST['unaaa_resolucion_nonce'], 'unaaa_resolucion_add')
			&& $_FILES['resol']['name']
		) {
			if(!$_FILES['resol']['error']) {
				$new_file_name = strtolower($_FILES['resol']['tmp_name']);

				require_once( ABSPATH . 'wp-admin/includes/image.php' );
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				require_once( ABSPATH . 'wp-admin/includes/media.php' );

				$fid = media_handle_upload( 'resol', 0 );

				if ( is_wp_error( $fid ) ) {
					wp_die('Error loading file!');
				 } else {
				   global $wpdb;
				   $table_name = $wpdb->prefix . 'unaaa_resol';
	   
				   $title = sanitize_text_field($_POST['title']);
				   $content = sanitize_text_field($_POST['content']);
				   $tipo = (int)$_POST['tipo'];
				   $fecha = $_POST['fecha'];
				   $status = 1;
				   $created_at = current_time('mysql');
				   $updated_at = current_time('mysql');

	   
				   $wpdb->insert($table_name, compact('title', 'fecha', 'fid', 'tipo', 'content', 'status', 'created_at', 'updated_at'));
				?>
				<div class="wrap">
				<h1 class="wp-heading-inline">Resoluciones</h1>
				<a href="<?php echo admin_url('admin.php?page=unaaa-resoluciones'); ?>" class="page-title-action">Regresar al listado</a>
				<hr class="wp-header-end">
				<div class="notice notice-success is-dismissible"><p>Agregado correctamente!</p></div>
				<?php
				 }
			}
		} else {
		?>
		<div class="wrap">
			<h1 class="wp-heading-inline">Nueva resolucion</h1>
			<hr class="wp-header-end">


				<form method="post" enctype="multipart/form-data">
				<table class="form-table">
					<tr>
						<th scope="row"><label for="tipo">Tipo</label></th>
						<td>
							<select name="tipo" required>
								<option value="2">Resolución Administrativa</option>
								<option value="3">Resolución Presidencial.</option>
								<option value="24">Resoluciones de Comisión Organizadora</option>
								<option value="25">Resoluciones de Actas Ordinarias</option>
								<option value="26">Resoluciones de Actas Extraordinarias</option>
							</select>
						</td>
					</tr>	
					<tr>
						<th scope="row"><label for="title">Nombre</label></th>
						<td><input name="title" type="text" id="title" class="regular-text" required></td>
					</tr>
					<tr>
						<th scope="row"><label for="content">Description</label></th>
						<td><textarea name="content" required cols="50" rows="20"></textarea></td>
					</tr>
					<tr>
						<th scope="row"><label for="content">Fecha</label></th>
						<td><input type="date" name="fecha" required /></td>
					</tr>
					<tr>
						<th scope="row"><label for="content">Archivo</label></th>
						<td><input type="file" name="resol" required /></td>
					</tr>
				</table>

				<input type="hidden" name="unaaa_resolucion_nonce" value="<?php echo wp_create_nonce('unaaa_resolucion_add'); ?>">
				<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Agregar"></p>
				</form>
		</div>
		<?php
		}
	}

	function unaaa_list_table_init() {

		$unaaaTable = new Unaaa_List_Table();
		$unaaaTable->prepare_items();
		?>
		<div class="wrap">
		<h1 class="wp-heading-inline">Resoluciones</h1>
		<a href="<?php echo admin_url('admin.php?page=unaaa-resoluciones&action=add'); ?>" class="page-title-action">Agregar</a>
		<hr class="wp-header-end">
			<form id="movies-filter" method="post">
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
				<?php
				$unaaaTable->search_box('search', 'search_id');
				$unaaaTable->display();
				?>
			</form>
		</div>
		<?php
	}

	public function unaaa_screen_options() {
		$option = 'per_page';
		$args = [
			'label' => 'Libros',
			'default' => 10,
			'option' => 'books_per_page'
		];
		add_screen_option( $option, $args );
	}

}
