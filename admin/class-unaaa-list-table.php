<?php

class Unaaa_List_Table extends WP_List_Table {
    
    private $table;
    private $table_data;
    private $dataTipos;

    public function __construct(){

        parent::__construct( [
            'singular' => 'Resolucion'
            ,'plural' => 'Resoluciones'
            ,'ajax' => false
        ] );

        $this->dataTipos = [
            2 => 'Resolución Administrativa'
            ,3 => 'Resolución Presidencial.'
            ,24 => 'Resoluciones de Comisión Organizadora'
            ,25 => 'Resoluciones de Actas Ordinarias'
            ,26 => 'Resoluciones de Actas Extraordinarias'
        ];
        
    }

    public function column_default($item, $column_name) {
        switch($column_name){
            case 'tipo':
                return $this->dataTipos[$item[$column_name]];
                break;
            case 'ID':
            case 'title':
            case 'content':
            case 'fecha':
            case 'fid':
            case 'created_at':
                // return date('d/m/Y', $item[$column_name]);
            default:
                return $item[$column_name];
                // return print_r($item,true);
        }
    }

    public function column_title($item) {
        
        $actions = array(
            'edit' => sprintf(
                '<a href="?page=%s&action=%s&resolucion=%d&token=%s">Edit</a>'
                ,$_REQUEST['page']
                ,'edit'
                ,$item['ID']
                ,wp_create_nonce('unaaa_resolucion_edit')
            ),
            'delete' => sprintf(
                '<a href="?page=%s&action=%s&resolucion=%d&token=%s">Delete</a>'
                ,$_REQUEST['page']
                ,'delete'
                ,$item['ID']
                ,wp_create_nonce('unaaa_resolucion_remove')
            )
        );
        
        return sprintf(
            '%1$s <span style="color:silver">(fid:%2$s)</span>%3$s'
            ,$item['title']
            ,$item['ID']
            ,$this->row_actions($actions)
        );
    }

    public function column_cb($item) {
        return sprintf(
                '<input type="checkbox" name="element[]" value="%s" />',
                $item['ID']
        );
    }

    public function get_columns() {
        $columns = [
            'cb' => '<input type="checkbox" />'
            ,'ID' => __('ID')
            ,'title' => __('Title')
            ,'content' => __('Description')
            ,'fecha' => __('Date')
            ,'fid' => __('File')
            ,'tipo' => __('Type')
            ,'created_at' => __('Created')
        ];

        return $columns;
    }

    protected function get_sortable_columns() {
        $sortable_columns = [
              'ID' => ['ID', true]
              ,'title' => ['title', true]
              ,'fecha' => ['fecha', true]
              ,'tipo' => ['tipo', true]
        ];

        return $sortable_columns;
    }

    // protected function get_bulk_actions() {
    //     $actions = [
    //         'delete' => __('Delete')
    //         // ,'delete_all' => _('Delete all')
    //     ];

    //     return $actions;
    // }

    // public function process_bulk_action() {
        
    //     if( 'delete'===$this->current_action() ) {
    //         wp_die('Items deleted (or they would be if we had items to delete)!');
    //     }
        
    // }

    public function prepare_items() {

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $primary  = 'ID';
        $this->_column_headers = array($columns, $hidden, $sortable, $primary);
        $this->process_bulk_action();

        if ( isset($_POST['s']) ) {
            $this->table_data = $this->get_table_data($_POST['s']);
        } else {
            $this->table_data = $this->get_table_data();
        }
        usort($this->table_data, array(&$this, 'usort_reorder'));

        
        /* pagination */
        $per_page = 20;
        $current_page = $this->get_pagenum();
        $total_items = count($this->table_data);

        $this->table_data = array_slice($this->table_data, (($current_page - 1) * $per_page), $per_page);

        $this->set_pagination_args(array(
                'total_items' => $total_items,
                'per_page'    => $per_page,
                'total_pages' => ceil( $total_items / $per_page )
        ));

        $this->items = $this->table_data;
    }

    private function get_table_data($search = '') {
        global $wpdb;

        $this->table = $wpdb->prefix . 'unaaa_resol';

        if ( !empty($search) ) {
            return $wpdb->get_results(
                "SELECT * from {$this->table} WHERE title Like '%{$search}%'",
                ARRAY_A
            );
        } else {
            return $wpdb->get_results(
                "SELECT * from {$this->table}",
                ARRAY_A
            );
        }
    }

    private function usort_reorder($a, $b) {
        $orderby = (!empty($_GET['orderby'])) ? $_GET['orderby'] : 'fecha';
        $order = (!empty($_GET['order'])) ? $_GET['order'] : 'desc';
        $result = strcmp($a[$orderby], $b[$orderby]);
        return ($order === 'desc') ? $result : -$result;
    }

}